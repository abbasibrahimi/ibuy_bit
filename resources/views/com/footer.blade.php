<footer>
    <div class="well">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <label> Mobiles & Tablets: </label>
                </div>
                <div class="col-md-10">
                    <a href="#">Nokia Mobiles</a> | 
                    <a href="#">Samsung Mobiles</a> | 
                    <a href="#">Apple Iphones</a> | 
                    <a href="#">HTC Mobiles</a> | 
                    <a href="#">Huawei Mobiles</a> | 
                    <a href="#">Sony Mobiles</a> | 
                    <a href="#">Micromax Mobiles</a> | 
                    <a href="#">Samsung Tablets</a> | 
                    <a href="#">Apple Tablets</a> | 
                    <a href="#">Huawei Tablets</a> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label> Computers & Accessories : </label>
                </div>
                <div class="col-md-10">
                    <a href="#">HP Laptops</a> | 
                    <a href="#">Dell Laptops</a> | 
                    <a href="#">Acer Laptops</a> | 
                    <a href="#">Samsung Laptops</a> | 
                    <a href="#">Canon Cameras</a> | 
                    <a href="#">Nikon Cameras</a> | 
                    <a href="#">Sony Cameras</a> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>Watches : </label>
                </div>
                <div class="col-md-10">
                    <a href="#">Casio Watches</a> | 
                    <a href="#">Titan Watches </a> | 
                    <a href="#">Fastrack Watches</a> | 
                    <a href="#">Timex Watches</a> | 
                    <a href="#">Maxima Watches</a> | 
                    <a href="#">Citizen Watches</a> | 
                    <a href="#">Sonata Watches</a> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>Shoes & Footware : </label>
                </div>
                <div class="col-md-10">
                    <a href="#">Bata Shoes</a> | 
                    <a href="#">Woodland Shoes</a> | 
                    <a href="#">Fila Shoes</a> | 
                    <a href="#">Crocs Shoes</a> | 
                    <a href="#">Nike Shoes</a> | 
                    <a href="#">Adidas Shoes</a> | 
                    <a href="#">Puma Shoes</a> | 
                    <a href="#">Reebok Shoes</a> 
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <label>Get To Know Us</label>
                    <div class="col-lg-9">
                        <tr>
                            <td><a href="#">About Us</a></td><br>
                        <td><a href="#">Careers</a></td><br>
                        <td><a href="#">Press Releases</a></td>
                        </tr>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Connect With Us</label>
                    <div class="col-lg-9">
                        <tr>
                            <td><a href="#">About Us</a></td><br>
                        <td><a href="#">Careers</a></td><br>
                        <td><a href="#">Press Releases</a></td>
                        </tr>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Make Money With Us</label>
                    <div class="col-lg-9">
                        <tr>
                            <td><a href="#">About Us</a></td><br>
                        <td><a href="#">Careers</a></td><br>
                        <td><a href="#">Press Releases</a></td>
                        </tr>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Customer Services</label>
                    <div class="col-lg-9">
                        <tr>
                            <td><a href="#">About Us</a></td><br>
                        <td><a href="#">Careers</a></td><br>
                        <td><a href="#">Press Releases</a></td>
                        </tr>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</footer>
<script src="{!! URL::to('js/jquery.flexslider.js') !!}"></script>
<script src="{!! URL::to('js/modernizr.js') !!}"></script>