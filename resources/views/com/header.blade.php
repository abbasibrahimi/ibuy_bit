<head>
    <title>IBuy</title>
    <!--<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="{!! URL::to('css/bootstrap.min.css') !!}"/>
    <link rel="stylesheet" href="{!! URL::to('css/app.css') !!}"/>
    <link rel="stylesheet" href="{!! URL::to('css/flexslider.css') !!}"/>
    
    <script src="{!! URL::to('js/jquery.1.11.js') !!}"></script>
    <script src="{!! URL::to('js/bootstrap.min.js') !!}"></script>
</head>