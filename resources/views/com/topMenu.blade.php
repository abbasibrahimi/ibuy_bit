<div class="nav navbar-static-top navbarCustom">
    <div class="container topAbout">
        <a href="#" class="pull-right">About IBUY</a>
    </div>
    <div class="container">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#">Sign In</a></li>
                <li><a href="#">Join Free</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">My IBUY<span class="caret"></span></a>
                    <ul class="dropdown-menu rightDropdownMenu">
                        <li><a href="#">Favorites</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><strong>Buy</strong></a>
                            <a href="#">History</a>
                        </li>
                        <li><a href="#">Manage Orders</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><strong>Sell</strong></a>
                            <a href="#">Manage Products</a>
                        </li>
                        <li><a href="#">Receive RFQ</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">For Buyers <span class="caret"></span></a>
                    <ul class="dropdown-menu rightDropdownMenu">
                        <li><a href="#">By Category</a></li>
                        <li><a href="#">By Price</a></li>
                        <li><a href="#">Offer Zone</a></li>
                        <li><a href="#">Get Quotations</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">For Suppliers <span class="caret"></span></a>
                    <ul class="dropdown-menu rightDropdownMenu">
                        <li><a href="#">Supplier</a></li>
                        <li><a href="#">Best Supplier</a></li>
                        <li><a href="#">Gold Supplier</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Customer Services <span class="caret"></span></a>
                    <ul class="dropdown-menu rightDropdownMenu">
                        <li><a href="#">I Am a Buyer</a></li>
                        <li><a href="#">I Am a Supplier</a></li>
                        <li><a href="#">I Am a New User</a></li>
                        <li><a href="#">Report Suspicious Behavior</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
    <!--<nav class="nav navbar-link">Link1</nav>-->
</div>