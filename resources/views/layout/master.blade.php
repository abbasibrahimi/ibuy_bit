<!DOCTYPE html>
<html>
    @include('com.header')
    <body>
            @include('com.topMenu')
        <div class="container-fluid mainContainer">
            @yield("sidebar")
            @yield("mainContent")
        </div>
    </body>
    @include('com.footer')
</html>
