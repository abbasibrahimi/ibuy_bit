@extends('layout.master')
@section('mainContent')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row" style="margin-left: 0px;">
                <form class="form-inline" action="#">
                    <select class="form-control SearchStyle radiusLessRight">
                        <option> Books</option>
                        <option> Men Cloths</option>
                        <option> Women Cloths</option>
                        <option> Toys & Kids</option>
                        <option> Sports</option>
                        <option> Mobile</option>
                        <option> Tablets</option>
                        <option> Computer</option>
                        <option> TV</option>
                    </select>
                    <input class="form-control SearchStyle radiusLess" type="text" placeholder="Search What You Want Here ... ">
                    <input class="form-control btn btn-primary SearchStyle radiusLessLeft" type="submit" value="Search" />
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="list-group ibuy_list_group">
                <a href="#" class="list-group-item">Books<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Men Cloths & Fasion<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Women Cloths & Fasion<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Toys & Kids<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Sports & Health<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Mobile & Tablets<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">Computer & Accessories<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">TV & Home Appliance<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#" class="list-group-item">All Categories<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                @include("com.childMenu")
            </div>
        </div>
        <div class="col-md-5">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="6"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{!! URL::to('img/01.jpg') !!}" alt="...">
                        <div class="carousel-caption">
                            Books
                        </div>
                    </div>
                    <div class="item">
                        <img src="{!! URL::to('img/02.jpg') !!}" alt="Electronics Photo">
                        <div class="carousel-caption">
                            Electronics
                        </div>
                    </div>
                    <div class="item">
                        <img src="{!! URL::to('img/03.jpg') !!}" alt="Electronics Photo">
                        <div class="carousel-caption">
                            Sports
                        </div>
                    </div>
                    <div class="item">
                        <img src="{!! URL::to('img/04.jpg') !!}" alt="Electronics Photo">
                        <div class="carousel-caption">
                            Sports
                        </div>
                    </div>
                    <div class="item">
                        <img src="{!! URL::to('img/05.jpg') !!}" alt="Electronics Photo">
                        <div class="carousel-caption">
                            Sports
                        </div>
                    </div>
                    <div class="item">
                        <img src="{!! URL::to('img/06.jpg') !!}" alt="Electronics Photo">
                        <div class="carousel-caption">
                            Sports
                        </div>
                    </div>
                    
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
            </div>
        </div>

        <div class="col-md-4">
            <p>
                In the second round of Emir Sher Ali Khan kingdom,   three lithographic   printing presses named Shamsnuhar press, Murtazavee press and Mustavavee press started their activities. The first printed publication in the history of Afghanistan called Shamsunahar printed in 1873 in the first mentioned press. The office of this publication along with administrative formation of the above three presses could be counted as the core of the present ministry of information and culture.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p>
                In the second round of Emir Sher Ali Khan kingdom,   three lithographic   printing presses named Shamsnuhar press, Murtazavee press and Mustavavee press started their activities. The first printed publication in the history of Afghanistan called Shamsunahar printed in 1873 in the first mentioned press. The office of this publication along with administrative formation of the above three presses could be counted as the core of the present ministry of information and culture.
                In the second round of Emir Sher Ali Khan kingdom,   three lithographic   printing presses named Shamsnuhar press, Murtazavee press and Mustavavee press started their activities. The first printed publication in the history of Afghanistan called Shamsunahar printed in 1873 in the first mentioned press. The office of this publication along with administrative formation of the above three presses could be counted as the core of the present ministry of information and culture.
                In the second round of Emir Sher Ali Khan kingdom,   three lithographic   printing presses named Shamsnuhar press, Murtazavee press and Mustavavee press started their activities. The first printed publication in the history of Afghanistan called Shamsunahar printed in 1873 in the first mentioned press. The office of this publication along with administrative formation of the above three presses could be counted as the core of the present ministry of information and culture.

            </p>
        </div>
    </div>
    <div class="row">
        <h3> اعلانات ویژه</h3>

        <div class="flexslider carousel">
            <ul class="slides">
                <li>
                    <div class="thumbnail"><img src="{!! URL::to('img/001.jpg') !!}"></div>
                    <h4>Image 1</h4>
                    <p>Image Price</p>
                </li>
                <li>
                    <div class="thumbnail"><img src="{!! URL::to('img/002.jpg') !!}"></div>
                    <h4>Image 1</h4>
                    <p>Image Price</p>
                </li>
                <li>
                    <div class="thumbnail"><img src="{!! URL::to('img/001.jpg') !!}"></div>
                    <h4>Image 1</h4>
                    <p>Image Price</p>
                </li>
            </ul>
        </div>
    </div>

</div>

@stop